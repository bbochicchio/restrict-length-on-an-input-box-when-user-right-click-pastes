function clipPastedText() {

  
 var toClip = document.getElementById("test").value;
 var clipLen = toClip.length;
  // Uncomment this code to see the untrimmed input it's length
  /* document.getElementById("clip").innerHTML = "Raw Input " + toClip.toString();
  document.getElementById("cLength").innerHTML = "Len " + clipLen.toString();*/

  /* Check to see "if" the pasted text exceeds the desired length. If it does, trim it from first character to the desired length with the substring function. Then assign that value to the input field.  */
  if (clipLen >= 20) {
    
    toClip = toClip.toString();
    toClip = toClip.substr(0, 20);
    document.getElementById("test").value = toClip;
    document.getElementById("result").innerHTML = "Shortened Input: " + toClip.toString();
  } else {
 document.getElementById("result").innerHTML = "Not long enough";
  }
 

}