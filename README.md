# README #

Need a script to trim the pasted input to a text box? This will do it.  

Current Version: v1.0

### What does this do? ###

**The problem:** Visitors were able to submit more characters than allowed to an input field. Even though measures were in-place to limit the input already. 

**How it was reproduced: ** The only way to reproduce the behavior was a "right-click" paste and no further interaction with the input field. 

**How it was fixed: ** I used the oninput() method instead of onpaste() to make a catch-all for when the content of the input box changes. So now if they paste or type, the string is checked to see if it exceeds the maximum length. If it does then it will trim it.  If the string does not exceed the length, it does nothing.  

**How can I customize it: ** Change the value in the *if (clipLen >= 20)* to the length you want to invoke the trim. Then adjust the value in *toClip.substr(0, 20);* to match.  


### How do I get set up? ###

Put the files into your dev environment or upload to your web server. 

There are commented code lines to help you debug the script should you get unexpected results. 


### Ideas for Changes ###
* Setup a variable for the clip length. This way only 1 value needs changed. 
* Unify word choices so the script, function and file name is consistent.


### Contribution guidelines ###

I'm not sure this project warrants additional direct contribution.  


### Who do I talk to? ###

If you have a comment, request or idea on how to improve this or make it more useful feel free to contact me.